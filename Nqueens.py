import numpy as np
import copy

class Nqueens:

    def __init__(self,size,solution):
        print('Nqueens ', size)
        self.size = size
        self.solutions = 0
        self.solution = solution
        self.solutions1D = []
        self.solve()

    def solve(self):
        if self.solution == 'backtracking':
            positions = np.zeros((self.size, self.size))
            self.place_queen_bck(positions, 0)
            print(self.solutions)
        elif self.solution == 'forwardchecking':
            positions = np.zeros((self.size, self.size))
            # positions = [-1] * self.size
            self.place_queen_forwardchecking(positions,0, 0)
            print(self.solutions)

    def place_queen_bck(self, positions, queen_number):
        if queen_number == self.size:
            self.solutions += 1
        else:
            for i in range(self.size-1, -1, -1):
                if self.check_position_bck(positions, i, queen_number):
                    positions[i, queen_number] = 1
                    new_positions = copy.deepcopy(positions)
                    self.place_queen_bck(new_positions, queen_number + 1)
                    positions[i, queen_number] = 0

    def check_position_bck(self, positions, row, col):
        for k in range(self.size):
            if positions[row, k] == 1 or positions[k, col] == 1:
                return False
            for l in range(self.size):
                if (k + l == row + col) or (k - l == row - col):
                    if positions[k, l] == 1:
                        return False
        return True

    def place_queen_forwardchecking(self,positions,row,queen_number):
        if queen_number == self.size:
            self.solutions += 1
        else:
            for k in range(self.size):
                # for n in range(self.size):
                if positions[k, queen_number] == 0:
                    new_positions = copy.deepcopy(positions)
                    new_positions = self.confirm_positions(new_positions, k, queen_number)
                    if self.is_solution_possbile(positions,queen_number):
                        self.place_queen_forwardchecking(new_positions, k, queen_number+1)

    def confirm_positions(self,positions,row,col):
        positions[row, col] = -1
        for i in range(self.size):
            for j in range(self.size):
                if i == row and positions[i, j] != -1:
                    positions[i, j] += 1
                elif j == col and positions[i, j] != -1:
                    positions[i, j] += 1
                elif row + j == i + col and positions[i, j] != -1:
                    positions[i, j] += 1
                elif row - j == i - col and positions[i, j] != -1:
                    positions[i, j] += 1
        return positions

    def is_solution_possbile(self,positions, col):
        for i in range(col+1, self.size):
            possible = False
            for row in range(self.size):
                if positions[row,i] == 0:
                    possible = True
            if not possible:
                return False
        return True

    # def parse_to_1DArr(self, positions):
    #     return np.argmin(positions, axis=0)
    #
    # def check_solution(self, positions):
    #     result = self.parse_to_1DArr(positions).tolist()
    #     if result in self.solutions1D:
    #         return False
    #     self.solutions1D.append(result)
    #     return True


    #
    # def place_queen(self,positions,target_row):
    #     if target_row == self.size:
    #         self.solutions += 1
    #     else:
    #         for i in range(self.size):
    #             if self.check_position(positions,target_row,i):
    #                 positions[target_row] = i
    #                 self.place_queen(positions, target_row + 1)
    #
    # def check_position(self, positions, ocuppied_rows, column):
    #     for i in range(ocuppied_rows):
    #         if positions[i] == column or \
    #                 positions[i] - i == column - ocuppied_rows or \
    #                 positions[i] + i == column + ocuppied_rows:
    #             return False
    #     return True

    #
    # def place_queen_forwardchecking(self,positions,queen_number):
    #     if queen_number == self.size:
    #         if self.check_solution(positions):
    #             self.solutions +=1
    #             print(self.solutions1D)
    #     else:
    #         # for i in range(self.size):
    #         for row in range(self.size):
    #             for col in range(self.size):
    #                 if positions[row,col] == 0:
    #                     new_positions = copy.copy(positions)
    #                     new_positions = self.confirm_positions(new_positions,row,col)
    #                     # print(new_positions)
    #                     self.place_queen_forwardchecking(new_positions,queen_number+1)
    #
    # def confirm_positions(self,positions,row,col):
    #     positions[row, col] = -1
    #     for i in range(self.size):
    #         for j in range(self.size):
    #             if i == row and positions[i, j] != -1:
    #                 positions[i, j] += 1
    #             if j == col and positions[i, j] != -1:
    #                 positions[i, j] += 1
    #             if row + j == i + col and positions[i, j] != -1:
    #                 positions[i, j] += 1
    #             if row - j == i - col and positions[i, j] != -1:
    #                 positions[i, j] += 1
    #     return positions
    #
    # def parse_to_1DArr(self, positions):
    #     return np.argmin(positions, axis=0)
    #
    # def check_solution(self, positions):
    #     result = self.parse_to_1DArr(positions).tolist()
    #     if result in self.solutions1D:
    #         return False
    #     self.solutions1D.append(result)
    #     return True


from Nqueens import Nqueens
from LatinSquare import LatinSquare
import time

start_time = time.time()


# LatinSquare(3,'backtracking')
# LatinSquare(3,'forwardchecking')

Nqueens(11,'backtracking')
# Nqueens(12,'forwardchecking')

print("--- %s seconds ---" % (time.time() - start_time))
import numpy as np
import copy
import random
import pprint

class LatinSquare:

    def __init__(self,size,solution):
        print('Latin Square :  ', size)
        self.size = size
        self.solutions = 0
        self.solution = solution
        self.solve()

    def solve(self):
        if self.solution == 'backtracking':
            positions = np.zeros((self.size, self.size))
            self.place_number_bck(positions, 0, 0)
            print(self.solutions)
        elif self.solution == 'forwardchecking':
            positions = np.zeros((self.size, self.size))
            can_be_set = np.full((self.size, self.size, self.size), True)
            self.place_number_fwd(positions, 0, 0, can_be_set)
            print(self.solutions)

    def place_number_bck(self, positions, row, col):
        if row == self.size:
            self.solutions += 1
        else:
            used_before = []
            for i in range(row,self.size):
                for k in range(col, self.size):
                    new_positions = copy.deepcopy(positions)
                    new_used_before = copy.deepcopy(used_before)
                    is_possible, new_positions, updated_used_before = self.put_if_possible(new_positions, row, col, used_before)
                    new_used_before.extend(updated_used_before)
                    if is_possible:
                        if col == self.size - 1:
                            self.place_number_bck(new_positions, row+1, 0)
                        else:
                            self.place_number_bck(new_positions, row, col+1)

    def put_if_possible(self,positions,row,col,used_before):
        row_possibilities = list(range(1, self.size + 1))
        col_possibilities = list(range(1, self.size + 1))
        for i in range(0, len(used_before)):
            if used_before[i] in row_possibilities:
                row_possibilities.remove(used_before[i])
                col_possibilities.remove(used_before[i])
        for i in range(self.size):
            if positions[row, i] in row_possibilities:
                row_possibilities.remove(positions[row, i])
            if positions[i, col] in col_possibilities:
                col_possibilities.remove(positions[i, col])
        possible_numbers = list(set(col_possibilities).intersection(row_possibilities))
        if possible_numbers:
            rand = random.randint(0, len(possible_numbers) - 1)
            positions[row, col] = possible_numbers[rand]
            used_before.append(possible_numbers[rand])
            return True, positions, used_before
        return False, None, used_before

    def place_number_fwd(self, positions, row, col, can_be_set):
        if row == self.size:
            self.solutions += 1
        else:
            used_before = []
            for i in range(row, self.size):
                for k in range(col, self.size):
                    new_positions = copy.deepcopy(positions)
                    new_used_before = copy.deepcopy(used_before)
                    new_can_be_set = copy.deepcopy(can_be_set)
                    is_possible, new_positions, updated_used_before = self.put_if_possible_fwd(new_positions, row, col, used_before)
                    new_used_before.extend(updated_used_before)
                    if is_possible:
                        new_can_be_set = self.prepare_sets(new_positions, new_can_be_set)
                        if col == self.size - 1:
                            new_row = row + 1
                            if new_row != self.size:
                                if self.check_if_solvable_fwd(new_can_be_set, row +1, 0):
                                    self.place_number_fwd(new_positions, row+1, 0, new_can_be_set)

                            else:
                                self.place_number_fwd(new_positions, row + 1, 0, new_can_be_set)
                        else:
                            if self.check_if_solvable_fwd(new_can_be_set, row, col+1):
                                self.place_number_fwd(new_positions, row, col+1, new_can_be_set)

    def put_if_possible_fwd(self,positions,row,col,used_before):
        row_possibilities = list(range(1, self.size + 1))
        col_possibilities = list(range(1, self.size + 1))
        for i in range(0, len(used_before)):
            if used_before[i] in row_possibilities:
                row_possibilities.remove(used_before[i])
                col_possibilities.remove(used_before[i])
        for i in range(self.size):
            if positions[row, i] in row_possibilities:
                row_possibilities.remove(positions[row, i])
            if positions[i, col] in col_possibilities:
                col_possibilities.remove(positions[i, col])
        possible_numbers = list(set(col_possibilities).intersection(row_possibilities))
        if possible_numbers:
            rand = random.randint(0, len(possible_numbers) - 1)
            positions[row, col] = possible_numbers[rand]
            used_before.append(possible_numbers[rand])

            return True, positions, used_before
        return False, None, used_before

    def prepare_sets(self, positions, can_be_set):
        for i in range(self.size):
            for k in range(self.size):
                for j in range(self.size):
                    if positions[i, k] != 0:
                        val = int(positions[i, k] - 1)
                        can_be_set[i,j,val] = False
                        can_be_set[j,k,val] = False
        return can_be_set

    def check_if_solvable_fwd(self, can_be_set, row, col):
        for i in range(0,self.size):
            if can_be_set[row,col,i] == True:
                return True
        return False


        # result = False
        # used_in_row = positions[row, :]
        # not_used_in_row = set(range(1, self.size + 1)) - set(used_in_row)
        # used_in_col = positions[:, col]
        # not_used_in_col = set(range(1, self.size + 1)) - set(used_in_col)
        # if row != self.size - 1 and col != self.size - 1:
        #     if set(not_used_in_col).intersection(not_used_in_row):
        #         result = True
        #     else:
        #         return False
        # else:
        #     return True
        #
        # return result

    #
    # used_in_row = positions[row, :]
    # used_in_col = positions[:, col]
    # not_used_in_row = set(range(1, self.size + 1)) - set(used_in_row)
    # not_used_in_col = set(range(1, self.size + 1)) - set(used_in_col)
    # if set(not_used_in_col).intersection(not_used_in_row):
    #     return True
    # return False

